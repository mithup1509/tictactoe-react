import React, { Component } from 'react';

class Buttons extends Component {
  
    render() { 
    
        return (
            <div className='buttons'>
                { this.props.onButtons.map((eachbutton)=>{
               return(<button style={{border:'1px solid black'}} className='btn' onClick={this.props.onOperation} key={eachbutton.id} id={eachbutton.id}>{eachbutton.value}</button>)
                })}
            </div>
        );
    }
}
 
export default Buttons;