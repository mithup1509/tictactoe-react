import React, { Component } from "react";
import Buttons from "./buttons";
import Winnerdeclaration from "./winnerdeclaration";
import "./tictactoe.css";

class Tictactoe extends Component {
  state = {
    condition: true,
    valuexory: "",
    buttonvalue: [
      { id: 1, value: "" },
      { id: 2, value: "" },
      { id: 3, value: "" },
      { id: 4, value: "" },
      { id: 5, value: "" },
      { id: 6, value: "" },
      { id: 7, value: "" },
      { id: 8, value: "" },
      { id: 9, value: "" },
    ],
    array: [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [6, 4, 2],
    ],
winner:""
  };

  operation = (e) => {
    this.noWinner();
    if (this.checking()) {
      if (this.state.valuexory.length === 0) {
        // console.log("log man");
        // console.log(this.state.buttonvalue[e.target.id-1].value);
        this.setState({ valuexory: "X" });
        let buttonvalue = [...this.state.buttonvalue];
        let index = buttonvalue.indexOf(
          this.state.buttonvalue[e.target.id - 1]
        ); //if button value have this object means it gives 0 and 0th index i put this object
        buttonvalue[index] = { ...this.state.buttonvalue[e.target.id - 1] };
        buttonvalue[index].value = "X";
        this.setState({ buttonvalue: buttonvalue });
      } else {
        if (
          this.state.valuexory === "X" &&
          this.state.buttonvalue[e.target.id - 1].value === "" &&
          this.state.condition
        ) {
          // console.log(this.state.condition);
          this.setState({ valuexory: "O" });
          let buttonvalue = [...this.state.buttonvalue];
          let index = buttonvalue.indexOf(
            this.state.buttonvalue[e.target.id - 1]
          ); //if button value have this object means it gives 0 and 0th index i put this object
          buttonvalue[index] = { ...this.state.buttonvalue[e.target.id - 1] };
          buttonvalue[index].value = "O";
          this.setState({ buttonvalue: buttonvalue });
        } else if (
          this.state.valuexory === "O" &&
          this.state.buttonvalue[e.target.id - 1].value === "" &&
          this.state.condition
        ) {
          // console.log(this.state.condition);
          this.setState({ valuexory: "X" });
          let buttonvalue = [...this.state.buttonvalue];
          let index = buttonvalue.indexOf(
            this.state.buttonvalue[e.target.id - 1]
          ); //if button value have this object means it gives 0 and 0th index i put this object
          buttonvalue[index] = { ...this.state.buttonvalue[e.target.id - 1] };
          buttonvalue[index].value = "X";
          this.setState({ buttonvalue: buttonvalue });
        }
      }
  
    }



  };

  checking = () => {
    let c=true;
    this.state.array.forEach((element) => {
        if (
          this.state.buttonvalue[element[0]].value !== "" &&
          this.state.buttonvalue[element[1]].value !== "" &&
          this.state.buttonvalue[element[2]].value !== ""
        ) {
          if (
            this.state.buttonvalue[element[0]].value ===
              this.state.buttonvalue[element[1]].value &&
            this.state.buttonvalue[element[1]].value ===
              this.state.buttonvalue[element[2]].value &&
            this.state.buttonvalue[element[2]].value ===
              this.state.buttonvalue[element[0]].value
          ) {
            c=false;
            this.setState({winner:`${this.state.buttonvalue[element[0]].value} is winner`})
          //   alert(`${this.state.buttonvalue[element[0]].value}  is winner`);
            return c;
          }
        }
      });
   



    
    return c;
  };

  noWinner=()=>{

    let variable=false;
    this.state.buttonvalue.forEach((element)=>{
        if(element.value === "") {
        variable=true;
        }
   })
   if(!variable ){
   let value= this.checking();
   if(value){
    this.setState({winner:`No Winner`})
    // alert("no winner");
   }
}
  }

restart=()=>{
    let buttonvalue=[...this.state.buttonvalue];
    buttonvalue.map((element)=>{
        element.value="";
    })
    this.setState({buttonvalue:buttonvalue});
    this.setState({winner:""});
}


  render() {

    //  this.state.condition = this.checking();   
    //  this.noWinner(); 
    
    return (
      <div className="container">
        <button className="restart" onClick={this.restart}>Restart</button>
        <Buttons
          onButtons={this.state.buttonvalue}
          onOperation={this.operation}
        />
    <Winnerdeclaration onwinner={this.state.winner}/>
      </div>
    );
  }
}

export default Tictactoe;


